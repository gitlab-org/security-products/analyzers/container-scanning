ARG UBI_IMAGE_MICRO=registry.access.redhat.com/ubi8/ubi-micro:8.10
ARG UBI_IMAGE=registry.access.redhat.com/ubi8/ubi-minimal:8.10

FROM registry.gitlab.com/gitlab-org/security-products/dependencies/build-images/ubi8-ruby-33 as gcs-builder
USER root
COPY . /gcs
WORKDIR /gcs
ENV PATH="/gcs/:${PATH}"
SHELL ["/bin/bash", "-c"]
RUN gem build gcs.gemspec -o gcs.gem

FROM ${UBI_IMAGE_MICRO} as initial

FROM ${UBI_IMAGE} as ruby-builder

ARG DNF_INSTALL_ROOT=/install-root
ARG DNF_OPTS_ROOT="--installroot=${DNF_INSTALL_ROOT}/ --setopt=reposdir=${DNF_INSTALL_ROOT}/etc/yum.repos.d/ --setopt=varsdir=/install-var/ --config=${DNF_INSTALL_ROOT}/etc/yum.repos.d/ubi.repo --setopt=cachedir=/install-cache/ --noplugins"
ARG UID=1000
ARG GID=1000
RUN mkdir -p ${DNF_INSTALL_ROOT}/ /install-var
COPY --from=initial / ${DNF_INSTALL_ROOT}/

COPY --from=ghcr.io/oras-project/oras:v0.12.0 /bin/oras ${DNF_INSTALL_ROOT}/usr/local/bin/
COPY --from=gcs-builder /gcs/gcs.gem /gcs/script/setup.sh /gcs/script/.bashrc /gcs/version ${DNF_INSTALL_ROOT}/home/gitlab/
COPY --from=gcs-builder /gcs/legal ${DNF_INSTALL_ROOT}/home/gitlab/legal/
COPY --from=gcs-builder /gcs/ee/LICENSE ${DNF_INSTALL_ROOT}/home/gitlab/ee/LICENSE

RUN microdnf install --best --refresh  --assumeyes --nodocs shadow-utils && \
    groupadd -g ${GID} gitlab -R ${DNF_INSTALL_ROOT}/ && \
    useradd gitlab -g ${GID} -u ${UID} -R ${DNF_INSTALL_ROOT}/ && \
    chown -R ${UID}:${GID}  ${DNF_INSTALL_ROOT}/home/gitlab/ && \
    chmod -R g=u ${DNF_INSTALL_ROOT}/home/gitlab && \
    chown ${UID}:${GID}  ${DNF_INSTALL_ROOT}/etc/pki/ && \
    chmod -R g+rw  ${DNF_INSTALL_ROOT}/etc/pki/

RUN microdnf update ${DNF_OPTS_ROOT} --best --refresh --assumeyes --nodocs && \
    microdnf module enable ${DNF_OPTS_ROOT} ruby:3.1 && \
    microdnf install ${DNF_OPTS_ROOT} --best --refresh  --assumeyes --nodocs \
    ruby util-linux wget curl tar git && \
    microdnf clean ${DNF_OPTS_ROOT} all

# Need this stage in order to run gem and install trivy. Could not have run gem in the previous stage
# as it was not able to access required files from install-root. Also could not have removed wget from
# the previous stage as it is required by the script in this stage. wget is removed in the next stage,
# hence the need for ubi-image stage.
FROM ${UBI_IMAGE_MICRO} as trivy-builder

ARG UID=1000
ARG DNF_INSTALL_ROOT=/install-root

COPY --from=ruby-builder  ${DNF_INSTALL_ROOT}/ /
USER ${UID}
RUN gem install --no-document /home/gitlab/gcs.gem && \
    export PATH=/home/gitlab:${PATH}; cd /home/gitlab && bash setup.sh

# Need this stage to remove wget package and oras
FROM ${UBI_IMAGE} as base

ARG DNF_INSTALL_ROOT=/install-root
RUN mkdir -p ${DNF_INSTALL_ROOT}/ /install-var
COPY --from=trivy-builder / ${DNF_INSTALL_ROOT}/
ARG DNF_OPTS_ROOT="--installroot=${DNF_INSTALL_ROOT}/ --setopt=reposdir=${DNF_INSTALL_ROOT}/etc/yum.repos.d/ --setopt=varsdir=/install-var/ --config=${DNF_INSTALL_ROOT}/etc/yum.repos.d/ubi.repo --setopt=cachedir=/install-cache/ --noplugins"
RUN microdnf remove ${DNF_OPTS_ROOT} wget && \
    microdnf clean ${DNF_OPTS_ROOT} all && \
    rm -f ${DNF_INSTALL_ROOT}/usr/local/bin/oras && \
    ln -s  /home/gitlab/bin/gtcs  ${DNF_INSTALL_ROOT}/usr/bin/gtcs && \
    rm -f ${DNF_INSTALL_ROOT}/var/lib/dnf/history*

FROM ${UBI_IMAGE_MICRO}

ARG DNF_INSTALL_ROOT=/install-root
ARG GID=1000

COPY --from=base ${DNF_INSTALL_ROOT}/ /

ENV PATH="/home/gitlab:${PATH}"
ENV HOME "/home/gitlab"
# Ensures proper encoding when manipulating files
ENV LANG=C.utf8

# https://docs.openshift.com/container-platform/4.6/openshift_images/create-images.html#support-arbitrary-user-ids
RUN chgrp -R ${GID} /home/gitlab /etc/pki && \
    chmod -R g=u /home/gitlab /etc/pki

USER gitlab
WORKDIR /home/gitlab

CMD ["gtcs", "scan"]
