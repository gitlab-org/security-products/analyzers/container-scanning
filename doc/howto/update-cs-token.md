# How to update expired CS_TOKEN

## Introductions

The `CS_TOKEN` is used to `push scanner binary updates` and make `gitlab api requests`. The token usually has a life span of 1 year. If the token expires we get pipeline failures for the various scheduled pipelines in the project. An example error message from the `trigger_db_update` scheduled pipeline looks like this:

```console
$ bundle exec rake trigger_db_update
{"message":{"base":["You do not have sufficient permission to run a pipeline on '5.5.9'. Please select a different branch or contact your administrator for assistance."]}}
```

## How to renew the CS_TOKEN

1. In the [Access tokens](https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning/-/settings/access_tokens), click `Add new token`
2. As a token name you can use `cs_token`. Choose an expiration date of 1 year and `api` scope. Role should be `maintainer`. Click `Create Project access token`
    - Note that the `maintainer` role is needed to run pipelines for protected branches. The release tags are protected and are what the scheduled pipelines run against.
3. Once you do this you can copy the access token.
4. Select `Settings > CI/CD > Variables`. Update the `CS_TOKEN` variable with the access token created above.
    - If the `CS_TOKEN` variable is not present for whatever reasons, use the configuration below to create a new variable:
        - Type: `Variable`
        - Environments: `All (default)`
        - Visibility: `Masked`
        - Check the flag `Protect variable`
        - Check the flag `Expand variable reference`
        - Set `Key` as `CS_TOKEN`
        - Set `Value` as the access token created above.
5. Go to Slack, choose `#g_secure-composition-analysis` and set a reminder. For example, if the expiration of the token is `2025-05-11`, set a reminder for a couple of days earlier. You can do this with the following command:

```
/remind #g_secure-composition-analysis Container Scanning CS_TOKEN is expiring on 2025/5/11. Follow the instructions from the relevant [doc](https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning/doc/howto/update-cs-token.md) to renew it on May 1st 2025
```
