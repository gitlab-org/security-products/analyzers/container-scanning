# frozen_string_literal: true

RSpec::Matchers.define :match_cyclonedx_schema do |version|
  # The cyclonedx json schema is taken from https://gitlab.com/gitlab-org/gitlab/-/blob/23c0d81f79abe5459d31d87f87099b5b39fedf14/app/validators/json_schemas/cyclonedx/bom-1.4.schema.json
  # in order to be consistent with what the monolith uses. The monolith's schema json has all the relative sub schema
  # references pulled into the same file because its validator doesn't currently have relative offset capabilities.
  def schema_for(version)
    relative_path = "spec/fixtures/bom-#{version}.schema.json"
    JSONSchemer.schema(Pathname.pwd.join(relative_path))
  end

  def validate(version, data)
    schema_for(version).validate(data).map { |error| JSONSchemer::Errors.pretty(error) }
  end

  match do |actual|
    !actual.nil? && (@errors = validate(version, actual.to_h)).empty?
  end

  failure_message do
    "didn't match the schema for #{version}. " \
      "The validation errors were:\n#{@errors.join("\n")}"
  end
end
