# frozen_string_literal: true

# TODO: DRY-up against other ruby files (e.g. Rakefile)
TRIVY_VERSION_FILE = './version/TRIVY_VERSION'

def legacy_schema?
  env['CS_SCHEMA_MODEL'] && env['CS_SCHEMA_MODEL'].to_i == 14
end

def property(properties, name)
  properties.find { |item| item["name"] == name }["value"]
end

RSpec.shared_examples 'as container scanner' do
  before(:all) do
    setup_schemas!
  end

  include_context 'with scanner'

  let(:max_seconds) { 51 }

  specify do
    expect(report).to match_security_report_schema(:container_scanning)
  end

  specify do
    skip 'remediations are EE only' if env['GITLAB_FEATURES'].empty?

    expect(report['remediations']).to be_present

    report['remediations'].each do |remedy|
      expect(remedy['summary']).not_to be_nil
      expect(remedy['diff']).not_to be_nil

      remedy['fixes'].each do |fix|
        expect(fix['cve']).not_to be_nil if legacy_schema?
        expect(fix['id']).not_to be_nil
      end
    end
  end

  specify do
    # the schema validation is not enough; see https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning/-/merge_requests/2678#note_851123724
    expect(report['vulnerabilities']).to be_present

    report['vulnerabilities'].each do |vulnerability|
      expect(vulnerability['id']).not_to be_nil
      expect(vulnerability['description']).not_to be_nil
      expect(vulnerability['severity']).not_to be_nil
      expect(vulnerability['location']['dependency']['package']['name']).not_to be_nil
      expect(vulnerability['location']['dependency']['version']).not_to be_nil
      expect(vulnerability['location']['operating_system']).not_to be_nil
      expect(vulnerability['location']['image']).not_to be_nil
      vulnerability['identifiers'].each do |id|
        expect(id['type']).not_to be_nil
        expect(id['name']).not_to be_nil
        expect(id['value']).not_to be_nil
        expect(id['url']).not_to be_nil
      end
      vulnerability['links'].each do |link|
        expect(link['url']).not_to be_nil
      end
    end

    if legacy_schema?
      expect(vulnerability['message']).not_to be_nil
      expect(report['vulnerabilities']).to all(include('category' => 'container_scanning'))
    end
  end

  shared_examples 'as trivy scanner' do
    specify do
      current_trivy_version = File.read(TRIVY_VERSION_FILE).strip

      if legacy_schema?
        expect(subject['vulnerabilities']).to all(include('scanner' => { 'id' => 'trivy', 'name' => 'trivy' }))
      end

      expect(report['scan']['scanner']['version']).to eql(current_trivy_version)
      expect(report['scan']['scanner']['id']).to eql('trivy')
      expect(report['scan']['scanner']['name']).to eql('Trivy')
      expect(report['scan']['scanner']['url']).to eql('https://github.com/aquasecurity/trivy/')
      expect(report['scan']['scanner']['vendor']['name']).to eql('GitLab')

      vendor_status = report['vulnerabilities'][0]['details']['vendor_status']
      expect(vendor_status).to include({ 'name' => 'Vendor Status', 'type' => 'text' })
      expect(vendor_status['value']).to be_present

      expect(sbom_scanning_report["bomFormat"]).to eq("CycloneDX")
      expect(sbom_scanning_report["version"]).to eq(1)
      expect(sbom_scanning_report["components"]).not_to be_nil
      expect(sbom_scanning_report["dependencies"]).not_to be_nil

      metadata_properties = sbom_scanning_report["metadata"]["properties"]
      expect(property(metadata_properties, "gitlab:meta:schema_version")).to eq("1")
      expect(property(metadata_properties, "gitlab:container_scanning:image:name")).not_to be_nil
      expect(property(metadata_properties, "gitlab:container_scanning:image:tag")).not_to be_nil
      expect(property(metadata_properties, "gitlab:container_scanning:operating_system:name")).not_to be_nil
      expect(property(metadata_properties, "gitlab:container_scanning:operating_system:version")).not_to be_nil
    end

    context 'with generated sbom' do
      subject(:sbom) { sbom_scanning_report }

      it { is_expected.to match_cyclonedx_schema('1.6') }
    end
  end

  specify do
    expect(report['scan']).not_to be_nil
    expect(report['scan']['end_time']).not_to be_nil
    expect(report['scan']['start_time']).not_to be_nil
    expect(report['scan']['status']).to eql('success')
    expect(report['scan']['type']).to eql('container_scanning')
  end

  it_behaves_like 'as trivy scanner'

  specify do
    start_time = DateTime.parse(subject['scan']['start_time']).to_time
    end_time = DateTime.parse(subject['scan']['end_time']).to_time

    expect(end_time.to_i - start_time.to_i).to be < max_seconds
  end

  specify do
    skip 'allowlisting is EE only' if env['GITLAB_FEATURES'].empty?

    allowlisted_vulnerability = report['vulnerabilities'].find do |v|
      v['identifiers'].any? { |i| i['value'] == 'CVE-2019-3462' }
    end

    expect(allowlisted_vulnerability).to be_nil
  end
end
